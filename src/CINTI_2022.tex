\documentclass[conference]{IEEEtran}
%\IEEEoverridecommandlockouts

\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{pythonhighlight}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{siunitx}

% \lstnewenvironment{PythonB}[1][]{\lstset{style=mypython, frame=none, #1}}{}

% \lstdefinestyle{Py2}{
%   style=mypython,
%   frame=none
%   }

\title{Smartphone--controlled industrial robots:\\Design and user performance evaluation}

\author{
    \IEEEauthorblockN{Adrienn De\'ak}
    \IEEEauthorblockA{\textit{Department of Electrical Engineering} \\
    \textit{Sapientia Hungarian University of Transylvania}\\
    Corunca, Romania \\
    deak.a.adrienn@student.ms.sapientia.ro}
    \and
    \IEEEauthorblockN{Zolt\'{a}n Sz\'{a}nt\'{o}, \'{A}ron Feh\'{e}r, L\H{o}rinc M\'{a}rton}
    \IEEEauthorblockA{\textit{Department of Electrical Engineering} \\
    \textit{Sapientia Hungarian University of Transylvania}\\
    Corunca, Romania \\
    \{zoltan.szanto, fehera, martonl\}@ms.sapientia.ro}
}

\newcommand\newsubcap[1]{\phantomcaption%
\caption*{\figurename~\thefigure(\thesubfigure): #1}}



\begin{document}

    \maketitle

    \begin{abstract}
        This work presents a solution for distant control of industrial robots using mobile devices. The developed application can monitor the motion of the robot based on video and sensor information, it can send commands and control scripts to the robot, and it ensures the jogging--like manual control of the robot. In addition to the aforementioned functionalities, the application can analyze the teleoperation performances of the user. A set of performance metrics were introduced to rate user performances. They are useful to evaluate and educate remote manual robot control techniques. Experimental measurements are also presented to show the applicability of the developed remote control application and user performance evaluation method.
    \end{abstract}

    \begin{IEEEkeywords}
        Telerobotics, Human--Robot Interface, User evaluation, Robot control, Mobile application, IoT
    \end{IEEEkeywords}


    \section{Introduction}
    Nowadays, IoT (Internet Of Things) technology-based systems are gaining more ground, enabling the efficient remote control,
    monitoring, and management of industrial processes with smart device applications.
    In the case of robotic systems, the monitoring and control through the Internet offers an opportunity to develop a wide range of new intelligent applications.
    For industrial systems, reliable supervision, rapid intervention, and proper access control are critical.
    % Hence, this kind of application development is a complex process.
    
    Industrial robots have their own control devices for programming and monitoring, but these tools can usually be used near the robot.
    Therefore, in addition to these devices, mobile phone applications could effectively serve as an extension.
    
    Due to the availability and high computational capabilities of smartphones, these devices seem to be a natural candidate for remote control and monitoring of industrial systems and robots \cite{DBLP:journals/corr/abs-1807-06003}, \cite{6561425}.
    They are capable of high--speed communication, and high-quality display of online video information.
    Moreover, a wide range of software tools is available for developing mobile applications with high complexity.
    However, especially due to the size and shape limitations, the online manual control with these devices is not a straightforward problem.
    The user interface, which facilitates the human--robot interaction, should be carefully designed to achieve efficient simultaneous video monitoring and manual robot actuation.
    
    In the case of mobile robot teleoperation, smartphones are popular for the implementation of the user--side application.
    Early results were presented e.g. in \cite{montufar2005simulating}, where the developed solution considers that teleoperation can be performed using phone tones or sending SMS messages.
    The paper \cite{Azeta_PM2019} describes an Android smartphone--based telerobotic system for surveillance applications that includes online video streaming.
    Another surveillance system that applies smartphone and a mobile robot was presented in \cite{7754547}.
    The paper \cite{6843630} presents the design and implementation of an Android application that focuses on manual control of mobile robots by applying Bluetooth communication.
    In the work \cite{6145963}, Android operating system--based robot platform, and smartphone--operated control and monitoring system was introduced.
    In the telerobotic system presented \cite{hasan2017smartphone}, a robotic arm was mounted on a smartphone--controlled mobile robot to perform more complex tasks such as collecting and holding work-pieces.
    
    Several approaches for smartphone--based control of robotic manipulators were also introduced.
    The study \cite{9144399} reviews the current human-robot interfaces and also presents a new approach for human--robot smartphone-based interfaces to operate robotic arms for assisting persons with disabilities in their active daily living tasks.
    The paper \cite{lutovac2015remote} presents a system for remote monitoring and control of industrial robots using Android device and Wi-Fi communication.
    The system ensures intuitive distant control by monitoring the robot's motion and by exploiting its three--dimensional model.
    In the paper \cite{8967973}, an augmented reality interface for human-robot interaction in a common working environment is proposed.
    Specifically, a smartphone-based augmented software solution was developed that allows a human operator to select any configuration within the workspace of the robot.
    The paper \cite{6935597} presents a new tablet--based user interface for industrial robot programming for various robotic tasks such as grinding, milling, or polishing.
    
    The wireless communication medium has a major influence on the behavior of telerobotic systems.
    The paper \cite{6646125} analyses through experiments the performance of a teleoperated robotic system in the function of communication quality.
    
    For industrial applications, it is important to reach nominal teleoperation performances for the proper execution of the task.
    Consequently, the evaluation of the operators that perform the distant control  is critical for industrial applications.
    
    In the view of the identified problems during the implementation of a smartphone--based telerobotic system, the research questions of this study are summarized as:
    \begin{itemize}
        \item What methods and techniques should be applied to develop an up-to-date smartphone--based user--friendly remote robot control software?
        \item Which performance metrics describe well the efficiency of the user during smartphone--based teleoperation?
        \item How the recorded user data can automatically be processed to compute these performance metrics?
    \end{itemize}

    The Universal Robot (UR) is a popular collaborative robot family with a wide range of industrial applications \cite{Kebria_SMC2016}.
    The smartphone-based teleoperation system presented in this paper is demonstrated on an UR--type robot but the proposed remote control design approach can be applied to a wide range of industrial robotic systems.

    A similar smartphone-based robot control application was described in the study \cite{CIONTOS_PM2019}.
    In contrast with this application, the solution presented in this study is also capable of online video monitoring, so it can be applied to remote control, direct visual contact is not necessary.
    Moreover, it is also able to automatically evaluate of the user by computing user-specific performance metrics.

    The research and development highlights of this paper can be summarized as:
    \begin{itemize}
        \item A smartphone application was developed for distant control of industrial robotic manipulators.
        The application allows both command--based control and online manual remote robot control using video information.
        The developed application uses state-of-the-art IoT technologies.
        \item A performance evaluation method was developed for such users that teleoperate a distant industrial robot based on video streaming.
    \end{itemize}

    In this view, the rest of the paper is organized as follows: Section \ref{sec:design} presents the mobile application design and implementation for distant robot control.
    In section \ref{sec:metrix} the performance metrics, proposed for user evaluation, are enumerated.
    Experimental measurements are presented in section \ref{sec:exp}.
    Finally, section \ref{sec:concl} concludes this study.

    \section{Distant Robot Control System Design} \label{sec:design}

    \subsection{Requirements and Problem Formulation} \label{subsec:sys_req}

    A typical remotely controlled robotic system consists of three components: a robot with local control equipment, a communication channel, and a user interface (UI) through which the operator can control the robot.
    Usually, these systems are also equipped with live camera feedback from the robot to aid the operator.
    Figure \ref{fig:logical} presents such a system along with the communication lines.

    From the operator, atomic commands or entire scripts, i.e. batch list of atomic commands, are forwarded to the robot.
    During task execution, the robot's internal state (configuration) modifies.
    These changes must be observable by the operator through the UI.
    In specific scenarios, it is helpful to have the robot and/or the environment equipped with sensors or cameras as they yield valuable information to the operator.

    \begin{figure}[h]
        \centering
        \includegraphics[width=0.6\linewidth]{img/logical}
        \caption{Block diagram of the remotely controlled robot system.}
        \label{fig:logical}
    \end{figure}

    The main components of a typical telerobotic system are:

    \subsubsection{Remotely controlled robot}

    In this work, we are focusing on remotely controlling a robotic manipulator.
    These robots are usually fixed in an environment, have a limited reach, i.e. working area,
    and perform tasks ranging from simple pick-and-place to complex assembly.
    The low-level control is handled by a particular computer in the robot's Control Box (CB) which is directly connected to the robot.
    The CB also manages all other robot-related I/O functions.
    Attached to the CB, we find a teach pendant which is a human--machine interface.
    Its main drawbacks are (i) physical size and movement limitation due to the direct connection, (ii) limited capacity to store data, and (iii) subtle customization offered.

    Often cameras are placed above the robot to get a bird's-eye view of the environment.
    If the arm's hardware allows, a camera is placed above robot's the end effector, which offers a closer view of the workspace.

    \subsubsection{Communication interface}

    It enables indirect communication between the user and the robot.
    Since the arm is constantly moving in the environment, wireless communication is preferred between the operator and the CB.

    The main requirements for a remotely controlled robot are:
    (i) low latency: the communication packets must arrive within a specific timeframe;
    (ii) low packet loss: preferably none, but strategies providing packet retransmission are also accepted; (iii) security: any packet alteration or eavesdropping done by a third party can lead to potential damages.

    The live camera feed transmission usually shares the communication medium for controlling and monitoring the robot.
    Depending on the camera settings (e.g. resolution), the video data will occupy a large chunk of the bandwidth,
    which might affect the previous requirements such as latency and packet loss.
    Although for a good user experience, it is essential to have a high-quality video feed and thus a high volume of data to be transmitted,
    this must not disturb the rest of the communication requirements, so they can function correctly.

    \subsubsection{User interface}

    The factory UI in the form of a teach pendant can serve as a basis for developing a much more mobile solution.
    Since smartphones are so prevalent, it is a natural choice to use them as UI devices.
    They instantly solve the mobility issue, but they also introduce new challenges like fitting all the information into a smaller screen.

    The UI must be capable of handling the system's primary use cases: sending commands to the robot, showing the current state of the robot, and also displaying the video feedback.
    Granting access only to authorized personnel is also a requirement.
    A final requirement is logging data into a database so that the information can be retrieved at a later point in time.

    \subsection{The proposed system architecture}
    \label{subsec:sys_arch}

    The proposed system architecture is presented in Figure~\ref{fig:arch_overall}.
    The main components are the mobile device-based UI, communication over ZMQ\footnote{\url{https://zeromq.org/}} and a Control Interface (CI),
    which handles the communication with the robot and camera.

    \begin{figure}[h]
        \centering
        \includegraphics[width=\columnwidth]{img/architecture}
        \caption{System architecture.}
        \label{fig:arch_overall}
    \end{figure}

    We selected an Android (SDK 10) smartphone for the UI since these devices are ubiquitous and easy to work with.
    Our developed application is called \emph{Smartel}, and it encompasses the UI functionalities.
    We use Firebase to store the gathered data as cloud-based databases offer greater storage capacity than the phone's internal memory.
    For each user interaction we store the user ID, the timestamp, the pressed button, the gripper state (open/closed), the joint states, button press time, the button release time, the robot's movement speed and the gripper position.
    Also, Firebase Authentication provides built-in methods to authenticate the users of the app.
    For authentication, we use a username and password combination assigned by a system administrator.

    Depending on the users' level of access/privileges, the application permits the following operation modes:
    (i) limited access in which the user has read-only access and
    (ii) full access to robot sensory data and video stream;
    allows sending instructions/scripts to the robot manipulator and enables manual control mode.

    We built our communication over the ZMQ protocol, an open-source, distributed messaging library.
    ZMQ is also cross-platform and supports multiple messaging patterns.
    Furthermore, it does not require a dedicated broker.

    In the proposed solution, we incorporated the two common types of communication patterns included in ZMQ:
    \begin{itemize}
        \item REQ/REP: here, the robot is requested to execute a command or script and to reply with the result.
        \item PUB/SUB: which is used by the CI to publish the state information and video feedback, respectively.
        The UI subscribes to each flow.
    \end{itemize}

    The CI acts as a gateway as it communicates via ZMQ with the UI, via USB3 with the camera, and via Ethernet IP with the CB.
    The CI is written in Python 3.7 running on a Windows 10 machine having \emph{pyzmq}\footnote{\url{https://pyzmq.readthedocs.io/en/latest/}} and \emph{ur-rtde}\footnote{\url{https://sdurobotics.gitlab.io/ur_rtde/index.html}} libraries installed.
    The former contains the bindings for ZMQ, while the latter contains the Real-Time Data Exchange (RTDE) interface bindings.
    The RTDE is built over a standard TCP/IP connection.
    It enables the monitoring and control of the robotic arm.

    In this work, we used the Universal Robots UR3, a lightweight, compact industrial robot.
    It has 6 degrees of freedom, and has a ±360$^{\circ}$ rotation on all wrist joints, making it ideal for table-top applications.

    As the UR3 allows mounting cameras directly on the arm, we placed the Intel RealSense D435\footnote{\url{https://www.intelrealsense.com/depth-camera-d435}} depth camera on the last joint.
    This setup offers a perfect view for grabbing objects with a gripper.
    For this project, we mainly use the RGB camera, but since switching to the depth camera is trivial, this can also aid e.g. pick and place tasks.
    
    The VidGear\footnote{\url{https://abhitronix.github.io/vidgear/v0.2.6-stable/}} which is a high-level wrapper around pyzmq library can be applied to further increase video transfer and processing performance.
    It is built over the Multi-Threaded + Asyncio API Framework providing real-time performance.
    Furthermore, VidGear has real-time JPEG Frame Compression capabilities for significantly boosting data transfer.

    \subsection{Working with the robot}

    The user can send MoveJ or MoveL instructions using the application.
    Figure \ref{fig:moveJ} presents a MoveJ command which provides references for all joints (Q1-Q6) in $[\text{rad}]$.
    Here we can also specify the execution velocity V $[\text{rad}/ s]$ and the acceleration A $[\text{rad}/s^2 ]$.
    The MoveL command can be used in a similar fashion setting the XYZ positions and orientations of the end effector in $[\text{mm}]$ and $[\text{rad}]$ respectively.
    In this case, the execution velocity V and the acceleration A values are given in $[\text{mm}/ s]$ and $[\text{mm}/s^2 ]$ respectively.
    In Figure \ref{fig:manual} the manual control menu is presented with the live camera feed, a track bar for setting the movement speed
    and buttons for positioning and orienting the end effector along the three Cartesian axes.
    The application allows sending stored scripts to the robot via the Run Script menu.

    An example script is shown in Figure \ref{fig:script}. Here \emph{accel\_mss}, \emph{speed\_ms}, \emph{blend\_radius} represents the linear acceleration, the linear velocity and the blending on sharp corners respectively.
    Each target is a vector containing 6 entries (gripper's position and orientation)
    The \emph{set\_digital\_out} configures the given port to the specified logical state.
    In this script we use \emph{movel} to perform jogging motion between the targets. 
    \begin{figure}
        \centering
        \begin{subfigure}[b]{.45\linewidth}
            \centering
            \includegraphics[width=.9\linewidth]{img/moveJ}
            \newsubcap{MoveJ command}
            \label{fig:moveJ}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{.45\linewidth}
            \centering
            \includegraphics[width=.9\linewidth]{img/manual}
            \newsubcap{Manual control}
            \label{fig:manual}
        \end{subfigure}
    \end{figure}

    \begin{figure}
        \begin{python}[frame=none]
pose_1 = [target_1, target_2, target_3, target_4]
pose_2 = [target_5, target_6, target_7, target_8]

accel_mss = 0.3
speed_ms = 0.3
blend_radius = 0.1
index = 0

set_digital_out(0, True)

while index < 4:
    movel(pose_1[i], accel_mss, 
            speed_ms, blend_radius)

    sleep(0.5)

    movel(pose_2[i], accel_mss,
            speed_ms, blend_radius)
    
    sleep(0.5)
    
    index = index + 1
end

set_digital_out(0, False)
        \end{python}
        \caption{Example of a used script for the UR3 robot.}
        \label{fig:script}
    \end{figure}


    \section{User Performance Evaluation Metrics} \label{sec:metrix}

    We have proposed a set of performance metrics to evaluate the user of the smartphone-based telerobotic system during video-based manual robot control.
    We analyze the users by observing the task execution speed and positioning accuracy. The metrics mainly focus on pick and place--like tasks.

    The user must perform a predefined command consisting of the movement of the robot along/around different axis based on video information.
    The metrics are determined by processing the data which is continuously saved during the task execution.
    The distance between the expected and realized arrival point coordinates is used to measure the accuracy of the user, which is divided into three parts:
    \begin{itemize}
        \item Positioning precision in \emph{x-y} plane is defined with the formula
        \begin{equation}
            \Delta_{xy} = \sqrt{(x_r - x_u)^2 + (y_r - y_u)^2}
        \end{equation}
        where ${x_r}$ the robot's prescribed position along the $x$ axis, ${x_u}$ the user's achieved position along the $x$ axis. The variables ${y_r}$ and ${y_u}$ denote the same along the $y$ axis.
        \item Positioning precision along $z$ axis is defined as
        \begin{equation}
            \Delta_{z} = |z_r - z_u|
        \end{equation}
        where ${z_r}$ is the $z$ coordinate of the robot's prescribed position, ${z_u}$ is the $z$ coordinate achieved by the user during command execution.
        \item Orientation precision: it is determined by the difference between the prescribed and realized orientation angles around the $z$ axis.
        Due to the symmetry of the gripper, the rotation can take place in two directions.
        The accuracy of the rotation can be determined by the formula
        \begin{multline}
            \Delta_{rotZ} = min(|rotZ1_r - rotZ_u|,|rotZ2_r - rotZ_u|),
        \end{multline}
        where ${rotZn_r}$ is the expected rotation angle of the robot around the $z$ axis and the ${rotZ_u}$ is the realized rotation angle around the $z$ axis by the user.
    \end{itemize}

    The rapidity of the user is determined by the execution time of a command sequence, which is the elapsed time between the start and the end of the task.
    It is defined by the equation
    \begin{equation}
        \delta t = t_E - t_S,
    \end{equation}
    where ${t_S}$ is the time instant when the task started and ${t_E}$ is the time instant when the task was ended.


    \section{Experimental Measurements} \label{sec:exp}

    \begin{figure}[!t]
        \centering
        \includegraphics[width=.9\columnwidth, keepaspectratio]{img/rendszer}
        \caption{Experimental setup}
        \label{fig:system}
    \end{figure}

    \subsection{Measurement methods}

    During the performance evaluation experiments, the users executed a predefined sequence of motions two times at three different robot speeds.
    Before starting the evaluation measurements, the user was allowed to navigate in the robot workspace using the manual control interface,
    which also includes the video display, to get acquainted with the environment.
    The experimental setup is presented in Figure \ref{fig:system}.

    Opening the gripper starts the measurement process locally in the application, and we saved the time of starting the measuring process.
    The command sequence included positioning in $x-y$ plane, positioning along $z$ axis, and rotation around $z$ axis.
    When the measurement is complete, the gripper is closed, after which the data is read from the database and the specified metrics are automatically calculated.
    
    The measurements (robot position information, start and stop time instances) are saved into a real-time Firebase database.

    The experiment involved twelve individuals, who were examined based on their manual control technique.
    All twelve users were able to execute the tasks after getting familiar with the working environment.
    After the learning phase, they performed the same sequence two times at three different speeds, so we worked with a total of 72 clear measurement data (24 data per speed).
    %Since users could try out the system before measuring, it was necessary to clean up the 92-data data set.
    Throughout the procedure, the communication delay between the user and the robot controller was negligible.

    \subsection{Measurement results}

    We visualized the measurement results according to the three robot speeds.
    Since each user takes two measurements per speed, by averaging these two results, we got six of the twelve data per user.

    The performance metrics at different speeds are presented in Figures \ref{fig:v005}, \ref{fig:v015} and \ref{fig:v03}.
    The measurements showed that the positioning accuracy is not correlated to the increase in execution time in the targeted robot speed range, it is user--dependent.
    The visualization also shows that the positioning along the $z$ is more precise compared to the positioning in the $x-y$ plane.

    \begin{figure}[h]
        \centering
        \includegraphics[width=1.1\columnwidth,keepaspectratio]{img/v005en.PNG}
        \caption{Performance metrics for all users (robot speed/maximum speed = 5\%)}
        \label{fig:v005}
    \end{figure}

    \begin{figure}[h]
        \centering
        \includegraphics[width=1.1\columnwidth,keepaspectratio]{img/v015en.PNG}
        \caption{Performance metrics for all users (robot speed/maximum speed = 15\%)}
        \label{fig:v015}
    \end{figure}

    \begin{figure}[h]
        \centering
        \includegraphics[width=1.1\columnwidth,keepaspectratio]{img/v03en.PNG}
        \caption{Performance metrics for all users (robot speed/maximum speed = 30\%)}
        \label{fig:v03}
    \end{figure}


    \section{Conclusions} \label{sec:concl}
    Besides safety and reliability, the user-friendliness of the control software is also important in industrial applications.
    An intuitive human--robot interface can increase the efficiency of task execution in the case of distant robot control.
    This paper proposed such a telerobotic system that allows the programming and manual control of industrial robot manipulators by using smartphones.
    The application uses recent IoT technologies such as ZMQ protocol, and Firebase Authentication.
    The behavior of the user is also critical during distant manual control of the robotic system.
    In this paper, a user performance evaluation method is also presented for video information--based distant manual control of industrial robots.
    The experimental measurement results show that the developed telerobotic system can efficiently be applied for remote robot manipulator control and user performance evaluation.

    The developed setup can be applied to perform such scientific quality experiments that involves human and robot interactions.

    \bibliographystyle{IEEETran}
    \bibliography{CINTI_2022}

%    \section*{Appendix}

%    The GitHub link of the Smartel telerobotics project: XXXX

%    The logo of the presented telerobotic application is presented in Figure \ref{fig:logo}.

%    \begin{figure}[!ht]
%        \centering
%        \includegraphics[width=.2\columnwidth, keepaspectratio]{img/logo}
%        \caption{The logo of the developed application}
%        \label{fig:logo}
%    \end{figure}

% that's all folks
\end{document}


